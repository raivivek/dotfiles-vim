Dotfiles-vim
============

The helpful plugin and settings of VIM that I use.

How to use
----------

```git clone https://github.com/vivekiitkgp/Dotfiles-vim ~/.vim```

This will clone the plugin into your local vim folder. Then we have to symlink vimrc to .vimrc.

```ln -s ~/Dotfiles-vim/vimrc .vimrc```


Enjoy VIM! 

Using plugins as submodules
---------------------------

The plugin in the vim folder can be initialized as submodules which makes it easy to upadte the plugins all at
once from their original authors git page. Here is a short instruction set on how to implement this feature. Please
note that in this repository of mine, the plguins are not submodules.

Suppose you want to install the fugitive plugin :

```
cd ~/.vim 
mkdir ~/.vim/bundle 
git submodule add http://github.com/tpope/vim-fugitive.git bundle/fugitive 
git add . 
git commit -m "Install Fugitive.vim bundle as a submodule."
```

It is assumed that your home vim directory is initialized as git repo. You can then push the changes to your remote
repo on github.

``` git push origin master ```

Installing on other machine
----------------------------

When you're on other machine, you simply need to pull the changes into your ```~/.vim``` folder.

``` git pull origin master ```

However at the moment there would the plugin folder, but no files within it. Initialize the folder using these commands :

``` 
git submodule init
git submodule upadte
```
or in a single line as ``` git submodule update --init ```

To Upgrade all the plugins (which have already been initialized):

``` git submodule foreach git pull origin master ```
